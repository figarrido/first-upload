from django.conf.urls import url, patterns
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView


urlpatterns = patterns("",
                       # Examples:
                       # url(r'^$', 'UpLoadImage.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       # url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', RedirectView.as_view(url='/home')),
                       url(r'^home/$', 'Upload.views.handle',
                           name='principal_page'),
                       ) + static(settings.MEDIA_URL,
                                  document_root=settings.MEDIA_ROOT)
