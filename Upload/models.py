from django.db import models
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class ModeloImagen(models.Model):
    photo = models.ImageField(upload_to=BASE_DIR + '/media/images/')
